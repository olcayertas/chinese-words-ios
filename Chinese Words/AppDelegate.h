//
//  AppDelegate.h
//  Chinese Words
//
//  Created by Cenk Orçun on 16.12.2015.
//  Copyright © 2015 ErakSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

