//
//  ViewController.m
//  Chinese Words
//
//  Created by Cenk Orçun on 16.12.2015.
//  Copyright © 2015 ErakSoft. All rights reserved.
//

#import "ViewController.h"
#import "Word.h"
#import <AFURLSessionManager.h>

@implementation ViewController {
    NSString *fileContent;
    NSArray *lines;
    NSMutableArray *words;
    int index;
    int action;
    Word *word;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    index = 0;
    action = 0;
    words = [NSMutableArray array];
}

- (void)viewDidAppear:(BOOL)animated {
    [self readFileFromDocuments];
}

- (void) downloadWords {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"http://www.olcayertas.com/words.txt"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL =
        [[NSFileManager defaultManager]
         URLForDirectory:NSDocumentDirectory
         inDomain:NSUserDomainMask
         appropriateForURL:nil
         create:NO error:nil];
        
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        [self readFileFromDocuments];
    }];
    
    [downloadTask resume];
}

- (void) readFileFromDocuments {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"words.txt"];
    fileContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    
    if (fileContent == nil) {
        NSLog(@"File not found! Downloading...");
        [self downloadWords];
    }
    else {
        //NSLog(@"File : \n %@", fileContent);
        [self parseFile];
    }
}

- (void) parseFile {
    
    lines = [fileContent componentsSeparatedByString:@"\n"];
    
    for (NSString *line in lines) {
        NSLog(@"Line %@\n", line);
        NSArray *parts = [line componentsSeparatedByString:@"-"];
        parts = [parts[1] componentsSeparatedByString:@"="];
        
        NSString *c = [parts[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *l = parts[1];
        
        Word *newWord = [[Word alloc] init];
        newWord.chinese = c;
        newWord.latin = l;
        
        [words addObject:newWord];
        
        NSLog(@"Chinese : %@", c);
        NSLog(@"Latin   : %@", l);
    }
    
    [self shuffle];
    [self setNextWord:index++];
}

- (void) setNextWord:(int)ind {
    word = [words objectAtIndex:index];
    [self.label_chinese setText:word.chinese];
    [self.button setTitle:@"Göster" forState:UIControlStateNormal];
}

- (void)shuffle {
    NSUInteger count = [words count];
    for (NSUInteger i = 0; i < count - 1; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [words exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

- (IBAction)onButtonClick:(id)sender {
    
    if (action == 0) {
        action = 1;
        [self.label_latin setText:word.latin];
        [self.button setTitle:@"Sıradaki" forState:UIControlStateNormal];
    } else {
        action = 0;
        [self.label_latin setText:@""];
        [self setNextWord:index++];
        
        if (index == words.count) {
            index = 0;
            [self shuffle];
            [self setNextWord:index];
        }
    }
}

- (IBAction)updateWords:(id)sender {
    [self.label_latin setText:@""];
    [self.label_chinese setText:@""];
    [self downloadWords];
}

@end
