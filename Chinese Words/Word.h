//
//  Word.h
//  Chinese Words
//
//  Created by Cenk Orçun on 20.12.2015.
//  Copyright © 2015 ErakSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Word : NSObject

@property (strong, nonatomic) NSString *chinese;
@property (strong, nonatomic) NSString *latin;

@end
