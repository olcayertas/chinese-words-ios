//
//  ViewController.h
//  Chinese Words
//
//  Created by Cenk Orçun on 16.12.2015.
//  Copyright © 2015 ErakSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label_chinese;
@property (weak, nonatomic) IBOutlet UILabel *label_latin;
@property (weak, nonatomic) IBOutlet UIButton *button;

- (IBAction)onButtonClick:(id)sender;
- (IBAction)updateWords:(id)sender;

@end

